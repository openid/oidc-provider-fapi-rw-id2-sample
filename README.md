# oidc-provider-fapi-rw-id2-sample

This is an example configuration that is used as part of the CI system to test
the FAPI conformance tests. It is not intended to be useful in any other
context.

## Prerequisites

**`config.json` file**

```
{
  "port": 3000,
  "issuer": "https://fapitest.com",
  "alias": "footest",
  "clientAuthType": "mtls",
  "fapiResponseMode": "plain_response"
}
```

- `port: number` The port an HTTPS server will run at inside the container
- `issuer: string` The Issuer Identifier value
- `alias: string` The alias to use for the Test Plan
- `clientAuthType: "mtls" | "private_key_jwt"` The client auth type variant value
- `fapiResponseMode: "plain_response" | "jarm"` The fapi response mode variant value


**server.key**

PEM formatted server private key used for TLS

**server.crt**

PEM formatted server certificate used for TLS


## Generating Test Plan configuration JSON
```console
# This outputs a Test Plan configuration JSON to stdout
docker run --rm \
  -v $(pwd)/config.json:/home/node/app/config.json \
  panvafs/oidc-provider-fapi-rw-id2-sample plan
```

## Running a server
```console
# apply other flags for naming the container and exposing the internal HTTPS server port
docker run -d \
  -v $(pwd)/config.json:/home/node/app/config.json \
  -v $(pwd)/server.key:/home/node/app/server.key \
  -v $(pwd)/server.crt:/home/node/app/server.crt \
  panvafs/oidc-provider-fapi-rw-id2-sample server
```
