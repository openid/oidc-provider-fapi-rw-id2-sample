/* eslint-disable camelcase */
const { readFileSync } = require('fs')
const path = require('path')

const jose = require('jose')

const normalize = (cert) => cert.replace(/(?:-----(?:BEGIN|END) CERTIFICATE-----|\s)/g, '')

const keying = (client_id) => {
  const cert = readFileSync(path.join(__dirname, `${client_id}.crt`), 'ascii')
  const key = readFileSync(path.join(__dirname, `${client_id}.key`), 'ascii')
  const keystore = new jose.JWKS.KeyStore(jose.JWK.asKey(key, { x5c: [normalize(cert)], alg: 'PS256', use: 'sig' }))

  return { keystore, mtls: { cert, key } }
}

module.exports = (client_id) => {
  const { keystore } = keying(client_id)
  return {
    client_id,
    // adding foobar uris here and two of them to require client_id on /authorize
    redirect_uris: ['https://example.com', 'https://example.com'],
    jwks: keystore.toJWKS()
  }
}

module.exports.plan = (client_id) => {
  const { keystore, mtls } = keying(client_id)
  return {
    client_id,
    scope: 'openid offline_access',
    jwks: keystore.toJWKS(true),
    mtls
  }
}
