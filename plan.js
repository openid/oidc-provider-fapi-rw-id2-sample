const { issuer: ISSUER, clientAuthType: CLIENT_AUTH_TYPE, fapiResponseMode: RESPONSE_MODE, alias: ALIAS } = require('./get_config')

let baseUrl = ISSUER
if (ISSUER.endsWith('/')) {
  baseUrl = ISSUER.slice(0, -1)
}

const { plan: generateClient } = require('./client')

const { mtls, ...client } = generateClient('tEQSOAi8JJbZLBYSKH5jQ')
const { mtls: mtls2, ...client2 } = generateClient('kRcKcv1HMqhR17qJttdGO')

const plan = {
  alias: ALIAS,
  description: `oidc-provider FAPI RW ID2 ${CLIENT_AUTH_TYPE} ${RESPONSE_MODE}`,
  server: {
    discoveryUrl: baseUrl + '/.well-known/openid-configuration'
  },
  client,
  mtls,
  client2,
  mtls2,
  resource: {
    resourceUrl: baseUrl + '/me'
  },
  browser: [
    {
      match: baseUrl + '/auth*',
      tasks: [
        {
          task: 'Login',
          optional: true,
          match: baseUrl + '/interaction*',
          commands: [
            ['text', 'name', 'login', 'foo', 'optional'],
            ['text', 'name', 'password', 'bar', 'optional'],
            ['click', 'class', 'login-submit']
          ]
        },
        {
          task: 'Consent',
          optional: true,
          match: baseUrl + '/interaction*',
          commands: [
            ['click', 'class', 'login-submit']
          ]
        },
        {
          task: 'Verify Complete',
          match: `*/test/a/${ALIAS}/callback*`,
          commands: [
            ['wait', 'id', 'submission_complete', 10]
          ]
        }
      ]
    }
  ],
  override: {
    'fapi-rw-id2-ensure-redirect-uri-in-authorization-request': {
      browser: [
        {
          comment: 'expect an immediate error page',
          match: baseUrl + '/auth*',
          tasks: [
            {
              task: 'Expect redirect uri mismatch error page',
              match: baseUrl + '/auth*',
              commands: [
                ['wait', 'xpath', '//*', 10, 'oops! something went wrong', 'update-image-placeholder']
              ]
            }
          ]
        }
      ]
    },
    'fapi-rw-id2-ensure-registered-redirect-uri': {
      browser: [
        {
          comment: 'expect an immediate error page',
          match: baseUrl + '/auth*',
          tasks: [
            {
              task: 'Expect redirect uri mismatch error page',
              match: baseUrl + '/auth*',
              commands: [
                ['wait', 'xpath', '//*', 10, 'oops! something went wrong', 'update-image-placeholder']
              ]
            }
          ]
        }
      ]
    },
    'fapi-rw-id2-ensure-request-object-without-redirect-uri-fails': {
      browser: [
        {
          comment: 'expect an immediate error page',
          match: baseUrl + '/auth*',
          tasks: [
            {
              task: 'Expect redirect_uri missing error page',
              match: baseUrl + '/auth*',
              commands: [
                ['wait', 'xpath', '//*', 10, 'oops! something went wrong', 'update-image-placeholder']
              ]
            }
          ]
        }
      ]
    },
    'fapi-rw-id2-user-rejects-authentication': {
      browser: [
        {
          comment: 'Rejects interaction',
          match: baseUrl + '/auth*',
          tasks: [
            {
              task: 'Clicks cancel',
              match: baseUrl + '/interaction*',
              commands: [
                ['click', 'xpath', '//*/a']
              ]
            }
          ]
        }
      ]
    }
  }
}

console.log(JSON.stringify(plan, null, 4))
