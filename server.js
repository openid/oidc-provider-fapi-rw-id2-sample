const { readFileSync, existsSync } = require('fs')
const { createServer } = require('https')
const { randomBytes } = require('crypto')
const { EOL } = require('os')

const { port: PORT, issuer: ISSUER, clientAuthType: CLIENT_AUTH_TYPE, fapiResponseMode: RESPONSE_MODE } = require('./get_config')

if (!existsSync('server.key')) {
  console.error('server.key file missing')
  process.exit(1)
}

if (!existsSync('server.crt')) {
  console.error('server.crt file missing')
  process.exit(1)
}

const SERVER_KEY = readFileSync('server.key')
const SERVER_CERTIFICATE = readFileSync('server.crt')

const Koa = require('koa')
const mount = require('koa-mount')
const { Provider } = require('oidc-provider')
const helmet = require('koa-helmet')
const jose = require('jose')

const client = require('./client')

const { pathname } = new URL(ISSUER)
const app = new Koa()

const ks = new jose.JWKS.KeyStore()
ks.generateSync('RSA', 2048, { alg: 'PS256', use: 'sig' })

const oidc = new Provider(ISSUER, {
  acrValues: ['urn:mace:incommon:iap:silver'],
  scopes: ['openid', 'offline_access'],
  cookies: {
    keys: [randomBytes(32)]
  },
  jwks: ks.toJWKS(true),
  features: {
    fapiRW: { enabled: true },
    mTLS: {
      enabled: true,
      certificateBoundAccessTokens: true,
      selfSignedTlsClientAuth: true,
      getCertificate (ctx) {
        const peerCertificate = ctx.socket.getPeerCertificate()
        if (peerCertificate.raw) {
          return `-----BEGIN CERTIFICATE-----${EOL}${peerCertificate.raw.toString('base64').match(/.{1,64}/g).join(EOL)}${EOL}-----END CERTIFICATE-----`
        }
      }
    },
    claimsParameter: { enabled: true },
    jwtResponseModes: { enabled: true },
    jwtUserinfo: { enabled: false },
    userinfo: { enabled: true },
    requestObjects: {
      request: true,
      requestUri: false,
      mergingStrategy: {
        name: 'strict'
      }
    }
  },
  tokenEndpointAuthMethods: [CLIENT_AUTH_TYPE === 'mtls' ? 'self_signed_tls_client_auth' : 'private_key_jwt'],
  whitelistedJWA: {
    authorizationSigningAlgValues: ['PS256'],
    idTokenSigningAlgValues: ['PS256'],
    requestObjectSigningAlgValues: ['PS256'],
    tokenEndpointAuthSigningAlgValues: ['PS256']
  },
  responseTypes: RESPONSE_MODE === 'jarm' ? ['code'] : ['code id_token'],
  clientDefaults: {
    authorization_signed_response_alg: 'PS256',
    grant_types: RESPONSE_MODE === 'jarm' ? ['authorization_code'] : ['authorization_code', 'implicit'],
    id_token_signed_response_alg: 'PS256',
    introspection_signed_response_alg: 'PS256',
    request_object_signing_alg: 'PS256',
    response_types: RESPONSE_MODE === 'jarm' ? ['code'] : ['code id_token'],
    scope: 'openid offline_access',
    tls_client_certificate_bound_access_tokens: true,
    token_endpoint_auth_method: CLIENT_AUTH_TYPE === 'mtls' ? 'self_signed_tls_client_auth' : 'private_key_jwt'
  },
  clockTolerance: 5,
  clients: [
    client('tEQSOAi8JJbZLBYSKH5jQ'),
    client('kRcKcv1HMqhR17qJttdGO')
  ]
})

{
  const orig = oidc.Client.prototype.redirectUriAllowed
  oidc.Client.prototype.redirectUriAllowed = function redirectUriAllowed (value) {
    if (orig.call(this, value)) {
      return true
    }

    try {
      const { pathname, search } = new URL(value)

      return pathname.startsWith('/test/a/') &&
        pathname.endsWith('/callback') &&
        (search === '' || search === '?dummy1=lorem&dummy2=ipsum')
    } catch (err) {
      return false
    }
  }
}

{
  const orig = oidc.interactionResult
  oidc.interactionResult = function patchedInteractionResult (...args) {
    if (args[2] && args[2].login) {
      args[2].login.acr = 'urn:mace:incommon:iap:silver'
    }

    return orig.call(this, ...args)
  }
}

app.use(helmet())
app.use(mount(pathname.length > 1 && pathname.endsWith('/') ? pathname.slice(0, -1) : pathname, oidc.app))

const server = createServer({
  key: SERVER_KEY,
  cert: SERVER_CERTIFICATE,
  requestCert: true,
  rejectUnauthorized: false
}, app.callback())

server.listen(PORT)
